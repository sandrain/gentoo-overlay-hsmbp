### Disclaimer
The ebuilds here are patched for my own use (with MacBook Pro 13 Retina). You
can use freely, but at your own risk.

### Local overlay
See https://wiki.gentoo.org/wiki/Overlay/Local_overlay.

### Fixed ebuilds:
* app-text/texlive-core
* media-gfx/sam2p
* dev-lang/lua

